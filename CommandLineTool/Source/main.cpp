//
//  main.cpp
//  CommandLineTool
//
//  Created by Tom Mitchell on 08/09/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include <iostream>
using namespace std;

void factorial( int enter);

int main()
{
    int enter;
    // user input here...
    std::cout << "enter a number: ";
    std::cin >> enter;
    
    factorial(enter);
    
    return 0;
}

void factorial( int enter)
{
     int factorial = 1;
    
        for(int i = enter; i > 0; --i)
        {
            factorial *= i;
        }
    std::cout << " = " << factorial << std::endl;
}
